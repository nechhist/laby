module gitlab.com/nechhist/laby

go 1.13

require (
	github.com/go-ozzo/ozzo-dbx v1.5.0
	github.com/go-ozzo/ozzo-routing/v2 v2.2.0
	github.com/go-ozzo/ozzo-validation/v3 v3.8.1
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.3.0
	github.com/qiangxue/go-env v1.0.0
	github.com/qiangxue/go-restful-api v1.1.1
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.13.0
	gopkg.in/yaml.v2 v2.2.7
)
