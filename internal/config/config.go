package config

import (
	"fmt"
	"io/ioutil"

	validation "github.com/go-ozzo/ozzo-validation/v3"
	"github.com/qiangxue/go-env"
	"gitlab.com/nechhist/laby/pkg/log"
	"gopkg.in/yaml.v2"
)

const (
	// ServerPortDefault the server port
	ServerPortDefault = 8080
	// ImageDirDefault Image Dir Default
	ImageDirDefault = "images"
)

// Config represents an application configuration.
type Config struct {
	// ServerPort the server port
	ServerPort int `yaml:"server_port" env:"SERVER_PORT"`
	// ImageDir Name Image Directory
	ImageDir string `yaml:"image_dir"`
}

// Validate validates the application configuration.
func (c Config) Validate() error {
	return validation.ValidateStruct(&c)
}

// Load returns an application configuration which is populated from the given configuration file and environment variables.
func Load(file string, logger log.Logger) (*Config, error) {
	// default config
	c := getDefaultConfig()

	// load from YAML config file
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	if err = yaml.Unmarshal(bytes, &c); err != nil {
		return nil, err
	}

	// load from environment variables prefixed with "APP_"
	if err = env.New("APP_", logger.Infof).Load(&c); err != nil {
		return nil, err
	}

	// validation
	if err = c.Validate(); err != nil {
		return nil, err
	}

	return &c, err
}

// Check Config
func (c *Config) Check() error {
	if c.ServerPort <= 0 {
		return fmt.Errorf("ServerPort is empty. Enter a Server Port")
	}

	if c.ImageDir == "" {
		return fmt.Errorf("ImageDir is empty. Enter a directory path")
	}

	return nil
}

func getDefaultConfig() Config {
	return Config{
		ServerPort: ServerPortDefault,
		ImageDir:   ImageDirDefault,
	}
}
