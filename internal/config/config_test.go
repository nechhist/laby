package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCheck(t *testing.T) {
	c := getDefaultConfig()
	assert.Nil(t, c.Check())
}

func TestPort(t *testing.T) {
	c := getDefaultConfig()
	c.ServerPort = 0
	assert.NotNil(t, c.Check())
}

func TestDir(t *testing.T) {
	c := getDefaultConfig()
	c.ImageDir = ""
	assert.NotNil(t, c.Check())
}
