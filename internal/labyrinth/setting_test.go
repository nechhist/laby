package labyrinth

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func getTestSetting() *Setting {
	return &Setting{
		Col:               ColDefault,
		Row:               RowDefault,
		Complexity:        ComplexityDefault,
		LabyrinthFileName: "images/test_labyrinth.png",
		TrackFileName:     "images/test_labyrinth_track.png",
	}
}

func TestRow(t *testing.T) {
	s := getTestSetting()

	arr := []int{RowMin, RowMin + 1, RowMax, RowMax - 1}
	for _, value := range arr {
		s.Row = value
		assert.Nil(t, s.check())
	}

	arr = []int{RowMin - 1, RowMax + 1}
	for _, value := range arr {
		s.Row = value
		assert.NotNil(t, s.check())
	}
}

func TestCol(t *testing.T) {
	s := getTestSetting()

	arr := []int{ColMin, ColMin + 1, ColMax, ColMax - 1}
	for _, value := range arr {
		s.Col = value
		assert.Nil(t, s.check())
	}

	arr = []int{ColMin - 1, ColMax + 1}
	for _, value := range arr {
		s.Col = value
		assert.NotNil(t, s.check())
	}
}

func TestComplexity(t *testing.T) {
	s := getTestSetting()

	arr := []int{ComplexityMin + 1, ComplexityMin, ComplexityMax - 1, ComplexityMax}
	for _, value := range arr {
		s.Complexity = value
		assert.Nil(t, s.check())
	}

	arr = []int{ComplexityMin - 1, ComplexityMax + 1}
	for _, value := range arr {
		s.Complexity = value
		assert.NotNil(t, s.check())
	}
}

func TestTrackFileName(t *testing.T) {
	s := getTestSetting()

	s.TrackFileName = ""
	assert.NotNil(t, s.check())
}

func TestLabyrinthFileName(t *testing.T) {
	s := getTestSetting()

	s.LabyrinthFileName = ""
	assert.NotNil(t, s.check())
}
