package labyrinth

import (
	"image"
	"image/color"
)

const (
	// LineSide line side in pixel
	LineSide = 2
	// CellSide cell side in pixel
	CellSide = 24
)

// Labyrinth Labyrinth structure
type Labyrinth struct {
	img     *image.RGBA
	cells   [][]Cell
	worms   []Worm
	setting *Setting
}

// newLabyrinth creating a new labirinth with the initial settings
func newLabyrinth(setting *Setting) *Labyrinth {
	rest := image.Rect(
		0,
		0,
		setting.Col*CellSide+(2*LineSide),
		setting.Row*CellSide+(2*LineSide),
	)
	lab := &Labyrinth{
		img:     image.NewRGBA(rest),
		setting: setting,
	}
	return lab
}

func (lab *Labyrinth) draw() {
	lab.drawBackGround()
	lab.drawCells()
	lab.drawCorner()
	lab.drawOutsideBorder()
}

func (lab *Labyrinth) drawBackGround() {
	for i := 0; i < CellSide*lab.setting.Row+(2*LineSide); i++ {
		for j := 0; j < CellSide*lab.setting.Col+(2*LineSide); j++ {
			lab.img.Set(j, i, ColorBGDefault)
		}
	}
}

func (lab *Labyrinth) drawCells() {
	for _, cells := range lab.cells {
		for _, cell := range cells {
			for _, p := range cell.points.right {
				lab.img.Set(p.x, p.y, p.color)
			}
			for _, p := range cell.points.top {
				lab.img.Set(p.x, p.y, p.color)
			}
			for _, p := range cell.points.left {
				lab.img.Set(p.x, p.y, p.color)
			}
			for _, p := range cell.points.bottom {
				lab.img.Set(p.x, p.y, p.color)
			}
		}
	}
}

func (lab *Labyrinth) drawCenterCells() {
	for _, cells := range lab.cells {
		for _, cell := range cells {
			for _, p := range cell.points.center {
				if p.color == ColorMainWorm {
					lab.img.Set(p.x, p.y, p.color)
				}
			}
		}
	}
}

func (lab *Labyrinth) drawOutsideBorder() {

	for x := 0; x < CellSide*lab.setting.Col+(2*LineSide); x++ {
		for y := 0; y < LineSide; y++ {
			lab.img.Set(x, y, ColorLineDefault)
		}
	}

	for x := 0; x < CellSide*lab.setting.Col+(2*LineSide); x++ {
		for y := CellSide*lab.setting.Row + LineSide; y < CellSide*lab.setting.Row+LineSide+LineSide; y++ {
			lab.img.Set(x, y, ColorLineDefault)
		}
	}

	for x := 0; x < LineSide; x++ {
		for y := 0; y < CellSide*lab.setting.Row+LineSide+LineSide; y++ {
			lab.img.Set(x, y, ColorLineDefault)
		}
	}

	for x := CellSide*lab.setting.Col + LineSide; x < CellSide*lab.setting.Col+LineSide+LineSide; x++ {
		for y := 0; y < CellSide*lab.setting.Row+LineSide+LineSide; y++ {
			lab.img.Set(x, y, ColorLineDefault)
		}
	}
}

func (lab *Labyrinth) drawCorner() {
	for _, cells := range lab.cells {
		for _, cell := range cells {
			for _, p := range cell.points.corner {
				lab.img.Set(p.x, p.y, p.color)
			}
		}
	}
}

func drawPoint(points []Point, color color.Color) []Point {
	var newPoints []Point
	for _, p := range points {
		newPoints = append(newPoints, Point{p.x, p.y, color})
	}
	return newPoints
}
