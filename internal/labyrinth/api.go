package labyrinth

import (
	"errors"
	"fmt"
	routing "github.com/go-ozzo/ozzo-routing/v2"
	"gitlab.com/nechhist/laby/internal/config"
	"strconv"
	"strings"
	"time"
)

// RegisterHandlers registers the handlers that perform labirinth.
func RegisterHandlers(r *routing.Router, cfg *config.Config) {
	r.Get("/generate/<params>", func(c *routing.Context) error {

		// get params
		params := strings.Split(c.Param("params"), "_")
		if len(params) != 3 {
			return errors.New("Bad parameters")
		}

		// set setting
		s := &Setting{}
		s.Col, _ = strconv.Atoi(params[0])
		s.Row, _ = strconv.Atoi(params[1])
		s.Complexity, _ = strconv.Atoi(params[2])
		prefixFileName := fmt.Sprintf("%s/%d_%d", cfg.ImageDir, time.Now().Unix(), randInt(0, 1e6))
		s.LabyrinthFileName = fmt.Sprintf("%s_labyrinth.png", prefixFileName)
		s.TrackFileName = fmt.Sprintf("%s_labyrinth_track.png", prefixFileName)
		if err := s.check(); err != nil {
			return err
		}

		if err := GenerateLabyrinth(s); err != nil {
			return err
		}

		data := struct {
			ImageLabyrinth string
			ImageTrack     string
		}{
			s.LabyrinthFileName,
			s.TrackFileName,
		}
		return c.Write(&data)
	})
}
