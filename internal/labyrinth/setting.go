package labyrinth

import (
	"fmt"
)

const (
	// ServerPort the server port
	ServerPort = 8080
	// ColDefault Col Default
	ColDefault = 30
	// RowDefault Row Default
	RowDefault = 45
	// ComplexityDefault Complexity Default
	ComplexityDefault = 5

	// RowMin Row Min
	RowMin = 25
	// RowMax Row Max
	RowMax = 100
	// ColMin Col Min
	ColMin = 20
	// ColMax Col Max
	ColMax = 50
	// ComplexityMin Complexity Min
	ComplexityMin = 0
	// ComplexityMax Complexity Max
	ComplexityMax = 6
)

// Setting structure
type Setting struct {
	Row               int
	Col               int
	Complexity        int
	LabyrinthFileName string
	TrackFileName     string
}

// check Setting laby
func (s *Setting) check() error {
	if s.Row < RowMin || s.Row > RowMax {
		return fmt.Errorf("Error. Enter the correct number of row. Min: %d, max: %d", RowMin, RowMax)
	}

	if s.Col < ColMin || s.Col > ColMax {
		return fmt.Errorf("Error. Enter the correct number of col. Min: %d, max: %d", ColMin, ColMax)
	}

	if s.Complexity < ComplexityMin || s.Complexity > ComplexityMax {
		return fmt.Errorf("Error. Enter the correct number of complexity. Min: %d, max: %d", ComplexityMin, ComplexityMax)
	}

	if s.LabyrinthFileName == "" {
		return fmt.Errorf("LabyrinthFileName is empty")
	}
	if s.TrackFileName == "" {
		return fmt.Errorf("TrackFileName is empty")
	}
	return nil
}
