package labyrinth

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func randInt(min int, max int) int {
	n := max - min
	if n > 0 {
		return min + rand.Intn(n)
	}
	return 0
}
