package labyrinth

import (
	"math/rand"
	"testing"
	"time"
)

func TestRandInt(t *testing.T) {
	rand.Seed(time.Now().UTC().UnixNano())
	min := 5
	max := 10

	for i := 0; i <= 100; i++ {
		r := randInt(min, max)
		if r < min || r > max {
			t.Errorf("Error Generate range: %v", r)
		}
	}
}
