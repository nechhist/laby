package labyrinth

import (
	"image/png"
	"os"
)

// createLabyrinthImage create Labyrinth Image
func (l *Labyrinth) createLabyrinthImage() error {
	f, err := os.OpenFile(l.setting.LabyrinthFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}

	l.draw()

	err = png.Encode(f, l.img)
	if err != nil {
		return err
	}

	return f.Close()
}

// createTrackImage create Track Image
func (l *Labyrinth) createTrackImage() error {
	f, err := os.OpenFile(l.setting.TrackFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}

	l.draw()
	l.drawCenterCells()

	err = png.Encode(f, l.img)
	if err != nil {
		return err
	}

	return f.Close()
}
