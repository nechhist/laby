package labyrinth

import "testing"

func TestCreateWorms(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createWorms()

	if len(lab.worms) == 0 {
		t.Errorf("Error CreateWorms. Not found worms")
	}
}

func TestCreateBasicWorm(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createBasicWorm()

	if lab.worms[0].mode != WormMain {
		t.Errorf("Error CreateBasicWorm. Not found main worm")
	}
}

func TestCreateFailWorm(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createFailWorm()

	if lab.worms[0].mode != WormFail {
		t.Errorf("Error CreateBasicWorm. Not found fail worm")
	}
}
