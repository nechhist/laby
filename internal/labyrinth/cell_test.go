package labyrinth

import (
	"math/rand"
	"testing"
	"time"
)

const TEST_COL = 1
const TEST_ROW = 1

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func TestCreateCells(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()
	if len(lab.cells) != lab.setting.Row {
		t.Errorf("Error count row: %d, want: %d", len(lab.cells), lab.setting.Row)
	}
	if len(lab.cells[0]) != lab.setting.Col {
		t.Errorf("Error count col: %d, want: %d", len(lab.cells), lab.setting.Row)
	}
}

func TestDeleteRandWall(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()
	lab.deleteRandWall()
	for row, cells := range lab.cells {
		if row > 0 && row < lab.setting.Row-1 {
			for col, cell := range cells {
				if col > 0 && col < lab.setting.Col-1 {
					if cell.walls.right && cell.walls.left && cell.walls.top && cell.walls.bottom {
						t.Errorf("Error delete random wall in cell row: %d, col: %d", row, col)
					}
				}
			}
		}
	}
}

func TestDeleteTopBorderCell(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()

	lab.deleteTopBorderCell(TEST_ROW, TEST_COL)
	if lab.cells[TEST_ROW][TEST_COL].walls.top != false {
		t.Errorf("Error delete top wall in cell row: %d, col: %d", TEST_ROW, TEST_COL)
	}
	if lab.cells[TEST_ROW-1][TEST_COL].walls.bottom != false {
		t.Errorf("Error delete bottom wall in cell row: %d, col: %d", TEST_ROW-1, TEST_COL)
	}
}

func TestDeleteBottomBorderCell(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()
	lab.deleteBottomBorderCell(TEST_ROW, TEST_COL)
	if lab.cells[TEST_ROW][TEST_COL].walls.bottom != false {
		t.Errorf("Error delete bottom wall in cell row: %d, col: %d", TEST_ROW, TEST_COL)
	}
	if lab.cells[TEST_ROW+1][TEST_COL].walls.top != false {
		t.Errorf("Error delete top wall in cell row: %d, col: %d", TEST_ROW+1, TEST_COL)
	}
}

func TestDeleteLeftBorderCell(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()
	lab.deleteLeftBorderCell(TEST_ROW, TEST_COL)
	if lab.cells[TEST_ROW][TEST_COL].walls.left != false {
		t.Errorf("Error delete left wall in cell row: %d, col: %d", TEST_ROW, TEST_COL)
	}
	if lab.cells[TEST_ROW][TEST_COL-1].walls.right != false {
		t.Errorf("Error delete right wall in cell row: %d, col: %d", TEST_ROW, TEST_COL-1)
	}
}

func TestDeleteRightBorderCell(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()
	lab.deleteRightBorderCell(TEST_ROW, TEST_COL)
	if lab.cells[TEST_ROW][TEST_COL].walls.right != false {
		t.Errorf("Error delete right wall in cell row: %d, col: %d", TEST_ROW, TEST_COL)
	}
	if lab.cells[TEST_ROW][TEST_COL+1].walls.left != false {
		t.Errorf("Error delete left wall in cell row: %d, col: %d", TEST_ROW, TEST_COL+1)
	}
}

func TestMarkCellCenter(t *testing.T) {
	lab := getTestLabyrinth()
	lab.createAllCells()
	wantColor := ColorRed
	lab.markCellCenter(TEST_ROW, TEST_COL, wantColor)
	points := lab.cells[TEST_ROW][TEST_COL].points.center
	for _, p := range points {
		if p.color != wantColor {
			t.Errorf("Error MarkCell in point x: %d, y: %d, color: %v, want: %v", p.x, p.y, p.color, wantColor)
		}
	}
}
