package labyrinth

import "image/color"

type Point struct {
	x     int
	y     int
	color color.Color
}

func getTopPoints(x, y int, color color.Color) []Point {
	var points []Point
	for i := x; i < x+CellSide; i++ {
		for j := y; j < y+LineSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	return points
}

func getBottomPoints(x, y int, color color.Color) []Point {
	var points []Point
	for i := x; i < x+CellSide; i++ {
		for j := y + CellSide - LineSide; j < y+CellSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	return points
}

func getLeftPoints(x, y int, color color.Color) []Point {
	var points []Point
	for i := x; i < x+LineSide; i++ {
		for j := y; j < y+CellSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	return points
}

func getRightPoints(x, y int, color color.Color) []Point {
	var points []Point
	for i := x + CellSide - LineSide; i < x+CellSide; i++ {
		for j := y; j < y+CellSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	return points
}

func getCenterPoints(x, y int, color color.Color) []Point {
	var points []Point
	for i := x + CellSide/2 - 2; i < x+CellSide/2+4; i++ {
		for j := y + CellSide/2 - 2; j < y+CellSide/2+4; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	return points
}

func getCornerPoints(x, y int, color color.Color) []Point {
	var points []Point
	for i := x; i < x+LineSide; i++ {
		for j := y; j < y+LineSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	for i := x + CellSide - LineSide; i < x+CellSide; i++ {
		for j := y; j < y+LineSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	for i := x; i < x+LineSide; i++ {
		for j := y + CellSide - LineSide; j < y+CellSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}
	for i := x + CellSide - LineSide; i < x+CellSide; i++ {
		for j := y + CellSide - LineSide; j < y+CellSide; j++ {
			points = append(points, Point{i, j, color})
		}
	}

	return points
}
