package labyrinth

import (
	"image/color"
)

var (
	ColorBlack       = color.RGBA{0, 0, 0, 255}
	ColorWhite       = color.RGBA{255, 255, 255, 255}
	ColorRed         = color.RGBA{255, 0, 0, 255}
	ColorGreen       = color.RGBA{0, 255, 0, 255}
	ColorBlue        = color.RGBA{0, 0, 255, 255}
	ColorBGDefault   = ColorBlack
	ColorLineDefault = ColorWhite
	ColorMainWorm    = ColorRed
)
