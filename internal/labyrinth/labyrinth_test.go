package labyrinth

import (
	"testing"
)

func TestNewLab(t *testing.T) {
	setting := getTestSetting()
	lab := newLabyrinth(setting)

	if lab.setting != setting {
		t.Errorf("Error newLab. Error setting")
	}
}

func getTestLabyrinth() *Labyrinth {
	setting := getTestSetting()
	return newLabyrinth(setting)
}
