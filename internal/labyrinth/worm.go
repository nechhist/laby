package labyrinth

import (
	"image/color"
	"math/rand"
)

const (
	WormMain = iota
	WormFail = iota
)

type Worm struct {
	mode     int
	beginRow int
	beginCol int
	endRow   int
	endCol   int
	color    color.Color
}

func (lab *Labyrinth) createWorms() {
	for i := 0; i < lab.setting.Complexity; i++ {
		lab.createFailWorm()
	}
	lab.createBasicWorm()
}

func (lab *Labyrinth) createBasicWorm() {
	var wormTop Worm
	wormTop.mode = WormMain
	wormTop.beginRow = 0
	wormTop.beginCol = randInt(1, lab.setting.Col-2)
	wormTop.endRow = randInt(2, lab.setting.Row-(2*lab.setting.Complexity))
	wormTop.endCol = randInt(1, lab.setting.Col-2)
	wormTop.color = ColorMainWorm
	lab.worms = append(lab.worms, wormTop)
	//fmt.Println("wormTop: (" , wormTop.beginRow, ", " , wormTop.beginCol, " x " , wormTop.endRow, ", " , wormTop.endCol, ")")

	var wormMiddle1 Worm
	wormMiddle1.mode = WormMain
	wormMiddle1.beginRow = wormTop.endRow
	wormMiddle1.beginCol = wormTop.endCol
	wormMiddle1.endRow = randInt(lab.setting.Row/3, lab.setting.Row/2)
	wormMiddle1.endCol = randInt(1, lab.setting.Col-2)
	wormMiddle1.color = ColorMainWorm
	lab.worms = append(lab.worms, wormMiddle1)
	//fmt.Println("wormMiddle: (" , wormMiddle.beginRow, ", " , wormMiddle.beginCol, " x " , wormMiddle.endRow, ", " , wormMiddle.endCol, ")")

	var wormMiddle2 Worm
	wormMiddle2.mode = WormMain
	wormMiddle2.beginRow = wormMiddle1.endRow
	wormMiddle2.beginCol = wormMiddle1.endCol
	wormMiddle2.endRow = randInt(lab.setting.Row/2, lab.setting.Row-3)
	wormMiddle2.endCol = randInt(1, lab.setting.Col-2)
	wormMiddle2.color = ColorMainWorm
	lab.worms = append(lab.worms, wormMiddle2)
	//fmt.Println("wormMiddle: (" , wormMiddle.beginRow, ", " , wormMiddle.beginCol, " x " , wormMiddle.endRow, ", " , wormMiddle.endCol, ")")

	var wormBottom Worm
	wormBottom.mode = WormMain
	wormBottom.beginRow = wormMiddle2.endRow
	wormBottom.beginCol = wormMiddle2.endCol
	wormBottom.endRow = lab.setting.Row - 1
	wormBottom.endCol = rand.Intn(lab.setting.Col-3) + 1
	wormBottom.color = ColorMainWorm
	lab.worms = append(lab.worms, wormBottom)
	//fmt.Println("wormBottom: (" , wormBottom.beginRow, ", " , wormBottom.beginCol, " x " , wormBottom.endRow, ", " , wormBottom.endCol, ")")
}

func (lab *Labyrinth) createFailWorm() {
	var worm Worm
	worm.mode = WormFail
	worm.beginRow = randInt(0, lab.setting.Row/2)
	worm.beginCol = rand.Intn(lab.setting.Col-3) + 1
	worm.endRow = randInt(lab.setting.Row/2, lab.setting.Row-3)
	worm.endCol = rand.Intn(lab.setting.Col-3) + 1
	worm.color = ColorGreen
	lab.worms = append(lab.worms, worm)
}

func (lab *Labyrinth) runAllWorms() {
	for _, worm := range lab.worms {
		lab.runWorm(worm)
	}
}

func (lab *Labyrinth) runWorm(worm Worm) {
	row := worm.beginRow
	col := worm.beginCol
	i := 0
	vector := TOP
	for {
		i++
		if (row == worm.endRow && col == worm.endCol) || i > 100 {
			break
		}

		vector, power := worm.getWormVector(row, col, vector)

		for p := 0; p < power; p++ {
			if vector == TOP {
				if lab.wormGoTop(row, col) {
					lab.markCellCenter(row, col, worm.color)
					row = row - 1
				}
			}
			if vector == RIGHT {
				if lab.wormGoRight(row, col) {
					lab.markCellCenter(row, col, worm.color)
					col = col + 1
				}
			}
			if vector == BOTTOM {
				if lab.wormGoBottom(row, col) {
					lab.markCellCenter(row, col, worm.color)
					row = row + 1
				}
			}
			if vector == LEFT {
				if lab.wormGoLeft(row, col) {
					lab.markCellCenter(row, col, worm.color)
					col = col - 1
				}
			}
		}

	}
}

func (worm Worm) getWormVector(row, col, lastVector int) (int, int) {
	r := randInt(0, 2)
	power := 1 //randInt(1, 2)

	if r == 0 {
		// vector row
		if worm.endRow < row {
			return TOP, power
		}
		if worm.endRow > row {
			return BOTTOM, power
		}
	}
	if r == 1 {
		// vector col
		if worm.endCol < col {
			return LEFT, power
		}
		if worm.endCol > col {
			return RIGHT, power
		}
	}

	vector := randInt(0, 3)
	if (lastVector == LEFT && vector == RIGHT) || (lastVector == RIGHT && vector == LEFT) {
		vector = randInt(0, 3)
	}
	if (lastVector == TOP && vector == BOTTOM) || (lastVector == BOTTOM && vector == TOP) {
		vector = randInt(0, 3)
	}
	return vector, power
}

func (lab *Labyrinth) wormGoTop(row, col int) bool {
	if row > 0 && row < lab.setting.Row-1 {
		if lab.cells[row][col].walls.top != true && randInt(0, 3) == 0 {
			return false
		}

		lab.cells[row][col].walls.top = false
		lab.cells[row-1][col].walls.bottom = false
		lab.deleteTopBorderCell(row, col)
		return true
	}
	return false
}

func (lab *Labyrinth) wormGoBottom(row, col int) bool {
	if row >= 0 && row < lab.setting.Row-1 {
		if lab.cells[row][col].walls.bottom != true && randInt(0, 3) == 0 {
			return false
		}

		lab.cells[row][col].walls.bottom = false
		lab.cells[row+1][col].walls.top = false
		lab.deleteBottomBorderCell(row, col)
		return true
	}
	return false
}

func (lab *Labyrinth) wormGoRight(row, col int) bool {
	if col > 0 && col < lab.setting.Col-1 {
		if lab.cells[row][col].walls.right != true && randInt(0, 3) == 0 {
			return false
		}

		lab.cells[row][col].walls.right = false
		lab.cells[row][col+1].walls.left = false
		lab.deleteRightBorderCell(row, col)
		return true
	}
	return false
}

func (lab *Labyrinth) wormGoLeft(row, col int) bool {
	if col > 0 && col < lab.setting.Col-1 {
		if lab.cells[row][col].walls.left != true && randInt(0, 3) == 0 {
			return false
		}

		lab.cells[row][col].walls.left = false
		lab.cells[row][col-1].walls.right = false
		lab.deleteLeftBorderCell(row, col)
		return true
	}
	return false
}
