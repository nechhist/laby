package labyrinth

import (
	"image/color"
	"math/rand"
)

const (
	TOP    = iota
	RIGHT  = iota
	BOTTOM = iota
	LEFT   = iota
)

type CellWall struct {
	top    bool
	right  bool
	bottom bool
	left   bool
}

type Cell struct {
	row    int
	col    int
	points CellPoints
	walls  CellWall
}

type CellPoints struct {
	corner []Point
	center []Point
	top    []Point
	right  []Point
	bottom []Point
	left   []Point
}

func (lab *Labyrinth) createAllCells() {
	var x = LineSide
	var y = LineSide
	var allCells = make([][]Cell, lab.setting.Row)
	for row := 0; row < lab.setting.Row; row++ {
		allCells[row] = make([]Cell, lab.setting.Col)

		for col := 0; col < lab.setting.Col; col++ {
			var corner = getCornerPoints(x, y, ColorLineDefault)
			var top = getTopPoints(x, y, ColorLineDefault)
			var right = getRightPoints(x, y, ColorLineDefault)
			var bottom = getBottomPoints(x, y, ColorLineDefault)
			var left = getLeftPoints(x, y, ColorLineDefault)
			var center = getCenterPoints(x, y, ColorLineDefault)
			cell := Cell{
				row: row,
				col: col,
				points: CellPoints{
					corner: corner,
					top:    top,
					bottom: bottom,
					left:   left,
					right:  right,
					center: center,
				},
				walls: CellWall{true, true, true, true},
			}

			allCells[row][col] = cell
			x += CellSide
		}

		x = LineSide
		y += CellSide
	}
	lab.cells = allCells
}

func (lab *Labyrinth) deleteRandWall() {
	for _, cells := range lab.cells {
		for _, cell := range cells {
			count := 0
			if cell.walls.top {
				count++
			}
			if cell.walls.bottom {
				count++
			}
			if cell.walls.left {
				count++
			}
			if cell.walls.right {
				count++
			}

			if count <= 2 {
				continue
			}

			randSize := rand.Intn(3)

			if randSize == TOP {
				lab.deleteTopBorderCell(cell.row, cell.col)
			}
			if randSize == BOTTOM {
				lab.deleteBottomBorderCell(cell.row, cell.col)
			}
			if randSize == LEFT {
				lab.deleteLeftBorderCell(cell.row, cell.col)
			}
			if randSize == RIGHT {
				lab.deleteRightBorderCell(cell.row, cell.col)
			}

		}
	}
}

func (lab *Labyrinth) deleteTopBorderCell(row, col int) {
	if row > 0 && row <= lab.setting.Row-1 {
		lab.cells[row][col].points.top = drawPoint(lab.cells[row][col].points.top, ColorBGDefault)
		lab.cells[row][col].walls.top = false

		lab.cells[row-1][col].points.bottom = drawPoint(lab.cells[row-1][col].points.bottom, ColorBGDefault)
		lab.cells[row-1][col].walls.bottom = false
	}
}

func (lab *Labyrinth) deleteBottomBorderCell(row, col int) {
	if row >= 0 && row < lab.setting.Row-1 {
		lab.cells[row][col].points.bottom = drawPoint(lab.cells[row][col].points.bottom, ColorBGDefault)
		lab.cells[row][col].walls.bottom = false

		lab.cells[row+1][col].points.top = drawPoint(lab.cells[row][col].points.bottom, ColorBGDefault)
		lab.cells[row+1][col].walls.top = false
	}
}

func (lab *Labyrinth) deleteLeftBorderCell(row, col int) {
	if col > 0 && col <= lab.setting.Col-1 {
		lab.cells[row][col].points.left = drawPoint(lab.cells[row][col].points.left, ColorBGDefault)
		lab.cells[row][col].walls.left = false

		lab.cells[row][col-1].points.right = drawPoint(lab.cells[row][col-1].points.right, ColorBGDefault)
		lab.cells[row][col-1].walls.right = false
	}
}

func (lab *Labyrinth) deleteRightBorderCell(row, col int) {
	if col >= 0 && col < lab.setting.Col-1 {
		lab.cells[row][col].points.right = drawPoint(lab.cells[row][col].points.right, ColorBGDefault)
		lab.cells[row][col].walls.right = false

		lab.cells[row][col+1].points.left = drawPoint(lab.cells[row][col+1].points.left, ColorBGDefault)
		lab.cells[row][col+1].walls.left = false
	}
}

func (lab *Labyrinth) markCellCenter(row, col int, color color.Color) {
	lab.cells[row][col].points.center = drawPoint(lab.cells[row][col].points.center, color)
}
