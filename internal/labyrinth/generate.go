package labyrinth

// GenerateLabyrinth Generate Labyrinth
func GenerateLabyrinth(setting *Setting) error {

	lab := newLabyrinth(setting)
	lab.createAllCells()
	lab.createWorms()
	lab.runAllWorms()
	lab.deleteRandWall()

	if err := lab.createLabyrinthImage(); err != nil {
		return err
	}
	if err := lab.createTrackImage(); err != nil {
		return err
	}

	return nil
}
