package main

import (
	"flag"
	"fmt"
	"github.com/go-ozzo/ozzo-routing/v2"
	"github.com/go-ozzo/ozzo-routing/v2/content"
	"github.com/go-ozzo/ozzo-routing/v2/cors"
	"github.com/go-ozzo/ozzo-routing/v2/file"
	"gitlab.com/nechhist/laby/internal/config"
	"gitlab.com/nechhist/laby/internal/errors"
	"gitlab.com/nechhist/laby/internal/healthcheck"
	"gitlab.com/nechhist/laby/internal/labyrinth"
	"gitlab.com/nechhist/laby/pkg/accesslog"
	"gitlab.com/nechhist/laby/pkg/graceful"
	"gitlab.com/nechhist/laby/pkg/log"
	"net/http"
	"os"
	"time"
)

// Version indicates the current version of the application.
var Version = "1.0.0"

var flagConfig = flag.String("config", "./config/local.yml", "path to the config file")

func main() {
	flag.Parse()

	fmt.Println(flagConfig)

	// create root logger tagged with server version
	logger := log.New().With(nil, "version", Version)

	// load application configurations
	cfg, err := config.Load(*flagConfig, logger)
	if err != nil {
		logger.Errorf("failed to load application configuration: %s", err)
		os.Exit(-1)
	}

	if err := cfg.Check(); err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	if err := mkDirImages(cfg.ImageDir, true); err != nil {
		logger.Error(err)
		os.Exit(-1)
	}

	// build HTTP server
	address := fmt.Sprintf(":%v", cfg.ServerPort)
	s := &http.Server{
		Addr:    address,
		Handler: buildHandler(logger, cfg),
	}

	// start the HTTP server with graceful shutdown
	go graceful.Shutdown(s, 10*time.Second, logger)
	logger.Infof("server %v is running at %v", Version, address)
	if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Error(err)
		os.Exit(-1)
	}
}

func buildHandler(logger log.Logger, cfg *config.Config) http.Handler {
	router := routing.New()

	router.Use(
		accesslog.Handler(logger),
		errors.Handler(logger),
		content.TypeNegotiator(content.JSON),
		cors.Handler(cors.AllowAll),
	)

	healthcheck.RegisterHandlers(router, Version)
	labyrinth.RegisterHandlers(router, cfg)

	// dir with image files
	router.Get("/*", file.Server(file.PathMap{
		"/images": "/images",
	}))

	return router
}

func mkDirImages(dirName string, remove bool) error {
	os.MkdirAll(dirName, 0777)

	if _, err := os.Stat(dirName); err == nil {
		if remove == true {
			dirRead, _ := os.Open(dirName)
			dirFiles, _ := dirRead.Readdir(0)

			for index := range dirFiles {
				fileHere := dirFiles[index]

				// Get name of file and its full path.
				nameHere := fileHere.Name()
				fullPath := dirName + "/" + nameHere

				// Remove the file.
				os.Remove(fullPath)
			}
		}
	}
	return nil
}
